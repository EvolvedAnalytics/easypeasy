Project Description
===================

* Our revolutionary startup "TrueStories" wants a web application where stories can be submitted and managed.
* A user must be able to join using the signup button:

![See static/img/joinscreen.png for an example](static/img/joinscreen_small.png)

* If a user already has an account he can just login:

![See static/img/loginscreen.png for an example](static/img/loginscreen_small.png)

* After the user is logged in she or he can click a link to submit a new story:

![See static/img/submitstoryscreen.png for an example](static/img/submitstoryscreen_small.png)

* Independent whether a user is logged in or not she or he can click the stories link and view a list of stories: 

![See static/img/submitstoryscreen.png for an example](static/img/storiesscreen_small.png)

* A user can have admin rights, admin users can delete stories. The admin property can be hardcoded, no management interface is required.

* We encourage the use of libraries and pre-exising code. (As long as the correct authors are credited and licenses are respected.)

* Code should be reusable and easy to understand. Maximize the help of tools like ORMs (We really like SQLAlchemy).

* Good luck! 


Expected project time
=====================

* Our highly trained python wizards could finish this in about an hour, although they might have taken shortcuts because the beer was getting warm and the pool was closing. Normal project time should be a few hours.

How to run
==========

Make sure you have the following pyhton packages:


Flask, flask-babel markdown flup requests flask-sitemap

(can be installed using `pip install <package name>`)

In a terminal goto the folder and run

```
python run.py

```

If you go to: `http://localhost:5000/`  you can test the website.
