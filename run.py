#############################################################
#                                                           #
# This is the main script that contains the settings and    #
# routes of the flask web application.                      #
#                                                           #
#############################################################

from flask import Flask, request, jsonify, render_template, url_for, redirect, session,send_from_directory
from werkzeug import secure_filename
from flask_sitemap import Sitemap

#from wtforms import form, fields, validators
#import json



# Flask webapp configuration
app = Flask(__name__)
app.config['SITEMAP_INCLUDE_RULES_WITHOUT_PARAMS'] = True
ext = Sitemap(app=app)

# The WSGI expects an "application" object.
application=app



app.debug = True
app.secret_key = 'gBaMsKeAlFVY3VWX9wyyZIglBpY3XheXwqBPTr2VNuGU9kH5xfZCTDfga3wjP5Jm0TUug9mmiCKZ53Oz'

# Create a static function in the template enginge. This makes it easier to link to files
app.jinja_env.globals['static'] = ( lambda filename: url_for('static', filename=filename))


#################################################################
#  Generic information pages
#################################################################

@app.route('/')
def home():
    return render_template('main/home.html')

@app.route('/stories')
def stories():
    return render_template('main/stories.html')


#################################################################
#  Various
#################################################################     

@app.route('/robots.txt')
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])

@ext.register_generator
def index():
    # Not needed if you set SITEMAP_INCLUDE_RULES_WITHOUT_PARAMS=True
    yield 'home', {}

if __name__ == "__main__":
    app.run()
